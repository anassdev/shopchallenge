package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/product")
@Api(tags = ["Product Controller"])
class ProductController() {
    @Autowired
    private lateinit var productService: ProductService
    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/{sku}", produces = ["application/json;charset=utf-8"])
    @ApiOperation(value = "Retrieve Product By sku")
    fun findProductBySku(
            @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping()
    @ApiOperation(value = "Retrieve Product list By list of skus")
    fun findProductsBySkus(@RequestParam skus: List<String>): ResponseEntity<List<ProductEntity>> {
        logger.info("Get all products by skus list")
        return ResponseEntity.ok(productService.findProductsBySkuList(skus))
    }

    @PostMapping()
    @ApiOperation(value = "Create Product")
    fun createProduct(@RequestBody productRequest: ProductRequest): ResponseEntity<ProductResponse> {
        val product = productService.createProduct(productRequest);
        logger.info("Create Product : ")
        return if(product == null) {
            logger.error("Product already exist : " + productRequest.sku)
            ResponseEntity.badRequest().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @PutMapping("/{sku}")
    @ApiOperation(value = "Update Product")
    fun updateProduct(@PathVariable("sku") sku: String, @RequestBody productRequest: ProductRequest): ResponseEntity<ProductResponse> {
        val product = productService.updateProduct(sku, productRequest);
        logger.info("Update Product : ")
        return if(product == null) {
            logger.error("Product not exist : " + productRequest.sku)
            ResponseEntity.badRequest().build();
        } else {
            ResponseEntity.ok(product);
        }
    }
}
