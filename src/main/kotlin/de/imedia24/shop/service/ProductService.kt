package de.imedia24.shop.service

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {

        val product = productRepository.findBySku(sku)
        if (product !== null) {
            return product.toProductResponse()
        }
        return null
    }

    fun findProductsBySkuList(skus: List<String>): List<ProductEntity> {
        return productRepository.findBySkus(skus)
    }

    fun createProduct(productRequest: ProductRequest): ProductResponse? {
        var findProduct = productRepository.findBySku(productRequest.sku)
        if (findProduct !== null) {
            return null
        }
        // we can use MapStruct or @Builder like java
        // TODO validate product request champs
        var product = ProductEntity()
        product.sku = productRequest.sku
        product.name = productRequest.name
        product.price= productRequest.price;
        product.description= productRequest.description?:"";
        product.quantity = productRequest.quantity
        return productRepository.save(product).toProductResponse()
    }

    fun updateProduct(sku: String, productRequest: ProductRequest): ProductResponse? {
        var product = productRepository.findBySku(sku)
        if (product === null) {
            return null
        }
        // we can use manuel mapper here or use MapStruct
        // TODO validate product request champs
        product.name = productRequest.name
        product.price = productRequest.price
        product.quantity = productRequest.quantity
        product.description = productRequest.description?:product.description

        return productRepository.save(product).toProductResponse()
    }
}
