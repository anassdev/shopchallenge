package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal

class ProductRequest (
     val sku : String,
     val name: String,
     val price: BigDecimal,
     val description: String?,
     var quantity : BigDecimal
){
    companion object {
        fun ProductEntity.toProductRequest() = ProductRequest(
                sku = sku,
                name = name,
                description = description ?: "",
                price = price,
                quantity = quantity
        )
    }
}