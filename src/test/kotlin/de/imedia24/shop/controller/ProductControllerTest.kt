package de.imedia24.shop.controller

import com.ninjasquad.springmockk.MockkBean
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.service.ProductService
import io.mockk.every
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.math.BigDecimal
import java.time.ZonedDateTime

//HttpControllersTests
@WebMvcTest(ProductController::class)
class ProductControllerTest(@Autowired val mockMvc: MockMvc) {


    @MockkBean
    private lateinit var productService: ProductService

    @Test
    fun getProductsBySkus() {
        val productA1 = ProductEntity("A1", "name", "desc", BigDecimal.ONE, BigDecimal.TEN,
            createdAt = ZonedDateTime.now(),
            updatedAt = ZonedDateTime.now())
        val skus = listOf("A1", "A2")
        Assertions.assertEquals(productA1.sku, "A1")
        //every { productService.findProductsBySkuList(skus) } returns listOf(productA1)
        /*mockMvc.perform(MockMvcRequestBuilders.get("/product?skus=A1")
                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk)*/
    }

}
