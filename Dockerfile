FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD /build/libs/shop-0.0.1-SNAPSHOT.jar dockerShop.jar
ENTRYPOINT ["java", "-jar", "dockerShop.jar"]