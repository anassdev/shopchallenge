# iMedia24 Coding challenge

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Kotlin documentation](https://kotlinlang.org/docs/home.html)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/#build-image)
* [Flyway database migration tool](https://flywaydb.org/documentation/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

### Run application With Docker

Check if Docker is running:
    
    $ docker -v
    $ docker stats // this will list all the docker containers running
    $ docker images // this will list all the local docker images with tags 
Now, we firstly need to create the jar file of our build, for that open terminal in IntelliJ or from terminal, go to the source directory of the project:
    
    $ ./gradlew build
    // we will see that war file is created as in below image
    
Now we will create the jar file with command
    
    $ ./gradlew bootJar
    // we will now see the jar file also got created as in below image
    
After this, as we have declared in Dockerfile, our jar file is created.
Now we will build docker image with command
    
    $ docker build . -t dockershop
    
Now we will run the docker image as below : 
  
    $ docker run -name shop -p 8081:8080 dockershop

Test your application in localhost:8081


